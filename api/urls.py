from django.urls import path
from api.views.resume_views import create_resume,add_section,add_subsection,update_resume,delete_resume,delete_subsection,delete_skills
from api.views.resume_views import add_contact,update_contact,add_skills,update_section,get_all_resume,update_subsection,delete_contact
from api.views.resume_views import update_skills,delete_section,set_summary,get_resume,export_pdf_resume
from api.views.auth_views import login,register,validate
from api.views.assistance_views import summarize,matchResume
urlpatterns = [
    path("auth/login",login),
    path("auth/register",register),
    path("auth/validate",validate),
    
    path("resume",export_pdf_resume),
    path("resume/all",get_all_resume),
    path("resume/add",create_resume),
    path("resume/update",update_resume),
    path("resume/delete",delete_resume),
    path("resume/summary",set_summary),
    
    path("section/add",add_section),
    path("section/update",update_section),
    path("section/delete",delete_section),
    
    path("subsection/add",add_subsection),
    path("subsection/update",update_subsection),
    path("subsection/delete",delete_subsection),
    
    path("contact/add",add_contact),
    path("contact/update",update_contact),
    path("contact/delete",delete_contact),
    
    path("skills/add",add_skills),
    path("skills/update",update_skills),
    path("skills/delete",delete_skills),

    path("assistance/summary",summarize),
    path("assistance/match",matchResume)
]
