from django.contrib import admin

from .models import Profile,Resume,Contact,Section,SubSection,Skills

admin.site.register(Profile)
admin.site.register(Resume)
admin.site.register(Contact)
admin.site.register(Section)
admin.site.register(SubSection)
admin.site.register(Skills)