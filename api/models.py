from django.db import models
from django.contrib.auth.models  import AbstractBaseUser
class Profile(AbstractBaseUser):
    profile_id=models.AutoField(primary_key=True)
    first_name=models.CharField(max_length=30)
    last_name=models.CharField(max_length=30)
    email=models.CharField(max_length=70)
    password=models.CharField(max_length=200)
    USERNAME_FIELD = 'email'

class Resume(models.Model):
    resume_id=models.AutoField(primary_key=True)
    name=models.CharField(max_length=30,default="name")
    job_description=models.TextField()
    match=models.FloatField()
    created_at=models.DateField()
    last_modified=models.DateField()
    summary=models.TextField()
    # ref to profile
    profile=models.ForeignKey(Profile,related_name="resumes",on_delete=models.CASCADE)

class Section(models.Model):
    section_id=models.AutoField(primary_key=True)
    name=models.CharField(max_length=30,default="name")
    # ref to resume
    resume=models.ForeignKey(Resume,related_name="sections",on_delete=models.CASCADE)

class SubSection(models.Model):
    subsection_id=models.AutoField(primary_key=True)
    name=models.CharField(max_length=50,default="name")
    started_at=models.DateField()
    ended_at=models.DateField()
    organization=models.CharField(max_length=60)
    description=models.TextField()
    # ref to section
    section=models.ForeignKey(Section,related_name="subsections",on_delete=models.CASCADE)

class Contact(models.Model):
    contact_id=models.AutoField(primary_key=True)
    first_name=models.CharField(max_length=100)
    last_name=models.CharField(max_length=100)
    phone=models.CharField(max_length=100)
    address=models.CharField(max_length=300)
    email=models.CharField(max_length=300)
    # ref to resume
    resume=models.ForeignKey(Resume,related_name="contact",on_delete=models.CASCADE)

class Skills(models.Model):
    skills_id=models.AutoField(primary_key=True)
    keywords=models.TextField()
    # ref to resume
    resume=models.ForeignKey(Resume,related_name="skills",on_delete=models.CASCADE)
