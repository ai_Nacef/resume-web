from .services.auth import validate_token
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt

def auth_required(func):
    
    @csrf_exempt
    @api_view(['POST','GET','DELETE','PUT'])
    def wrapper(*args):
        # get parameter (request)
        req=args[0]
        # get auth token from request
        try:
            token=req.headers['Authorization']
            # if not valid return response with code 401
            payload=validate_token(token)
            if(not payload):
                return Response({"message":"unauthorized"},status=status.HTTP_401_UNAUTHORIZED)
        except:
            return Response({"message":"unauthorized"},status=status.HTTP_401_UNAUTHORIZED)
        # if valid call add user to request object func with parameter and return value 
        req._request.session['user']=payload
        return func(req._request)
        
    return wrapper