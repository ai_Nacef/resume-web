from ..models import Resume
from ..serializers import ResumeSerializer
import replicate
import requests
from builder.settings import ASSISTANCE_URL
import json

def summary(resumeId):
    # get resume from database
    resume=Resume.objects.get(pk=resumeId)
    if(not resume):
        return False
    # serialize resume
    resume=ResumeSerializer(resume).data
    # generate prompt
    prompt="write a summary self introduction in two sentences about this resume:\n"
    prompt=f"{prompt}{resume['contact'][0]['first_name']} {resume['contact'][0]['last_name']}\nSkills:\n{resume['skills'][0]}"
    for section in resume['sections']:
        prompt=f"{prompt}\n{section['name']}"
        for subsection in section['subsections']:
            prompt=f"{prompt}\n{subsection['name']} {subsection['organization']}\n{subsection['description']}"
    # call replicate api
    res=callApi(prompt)
    return res

# r8_OJ5F5dYaRC9ioIX05awpOvK6GOm8xMz4F08QH
def callApi(prompt):
    # set token as env var
    res=""
    for event in replicate.run("meta/llama-2-13b-chat",input={"prompt": prompt},):
        res+=str(event)
    return res.split("\n")[-1]

def serviceMatchResume(resumeId):
    # get resume from database
    resume=Resume.objects.get(pk=resumeId)
    if(not resume):
        return False
    # serialize resume
    resumeJson=ResumeSerializer(resume).data
    # generate text
    resumeText=""
    resumeText=f"{resumeText}{resumeJson['contact'][0]['first_name']} {resumeJson['contact'][0]['last_name']}\nSkills:\n{resumeJson['skills'][0]}"
    resumeText=f"{resumeText}\nsummary:\n{resumeJson['summary']}"
    for section in resumeJson['sections']:
        resumeText=f"{resumeText}\n{section['name']}"
        for subsection in section['subsections']:
            resumeText=f"{resumeText}\n{subsection['name']} {subsection['organization']}\n{subsection['description']}"
    jobText=resumeJson['job_description']
    resp=requests.post(f"{ASSISTANCE_URL}match/",data={"resume":resumeText,"job":jobText})
    if(resp.status_code!=200):
        return False
    resp=json.loads(resp.text)
    resume.match=resp['score']
    resume.save()
    return resp['score']
    