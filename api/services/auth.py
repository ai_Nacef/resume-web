from ..models import Profile
from ..serializers import FullProfileSerializer
import jwt
import datetime
SECRET_KEY_AUTH="secret_key"
DATE_FORMAT="%y-%m-%d %H:%M:%S"
def service_login(email,password):
    result=Profile.objects.filter(email=email)
    if(len(result)==0):
        return None
    user=result[0]
    user=FullProfileSerializer(user).data
    if(password!=user['password']):
        return None
    # create a token and return
    payload={
        "profile_id":user['profile_id'],
        "email":user['email'],
        "exp":datetime.datetime.now()+datetime.timedelta(days=3),
        "iat":datetime.datetime.now()
    }
    print(payload)
    token=jwt.encode(payload,SECRET_KEY_AUTH,algorithm='HS256')
    return token

def service_signup(profile):
    # check if email is already in use
    result=Profile.objects.filter(email=profile['email'])
    if(len(result)!=0):
        return False
    # create a new user 
    p=Profile(
        email=profile['email'],
        first_name=profile['first_name'],
        last_name=profile['last_name'],
        password=profile['password'])
    p.save()
    # send back a 200 status
    return True


def validate_token(token):
    try:
        payload=jwt.decode(token,SECRET_KEY_AUTH,algorithms=['HS256'])
        return payload
    except:
        return False