from api.models import Profile,Resume,Contact,Skills,Section,SubSection
from datetime import datetime
from django.utils import timezone
from ..serializers import ResumeSerializer
from django.shortcuts import render

DATE_FORMAT="%Y-%m-%d"
def service_create_resume(resume):
    profile=Profile.objects.get(pk=resume['user_id'])
    #resume=resume['name']
    new_resume=Resume(
        name=resume['name'],
        job_description=resume['job_description'],
        match=0,
        created_at=timezone.now().date(),
        last_modified=timezone.now().date(),
        profile=profile
    )
    new_resume.save()
    return new_resume

def service_add_section(section):
    # get resume from database
    resume=Resume.objects.get(pk=section['resume_id'])
    # create section object
    new_section=Section(
        name=section['name'],
        resume=resume
    )
    # save to database
    new_section.save()
    # change last modified to now
    resume.last_modified=timezone.now().date()
    resume.save()
    return new_section

def service_add_subsection(subsection):
    section=Section.objects.get(pk=subsection['section_id'])
    started_at=datetime.strptime(subsection['started_at'],DATE_FORMAT).date() 
    
    ended_at=datetime.strptime(subsection['ended_at'],DATE_FORMAT).date() 
    
    new_subsection=SubSection(
        name=subsection['name'],
        started_at=started_at,
        ended_at=ended_at,
        organization=subsection['organization'],
        description=subsection['description'],
        section=section
    )
    new_subsection.save()
    return new_subsection


def service_set_contact(contact):
    # get resume from database
    resume=Resume.objects.get(pk=contact['resume_id'])
    # create contact object with resume as parameter
    contact=Contact(
        first_name=contact['first_name'],
        last_name=contact['last_name'],
        phone=contact['phone'],
        address=contact['address'],
        email=contact['email'],
        resume=resume
    )
    # save to database
    contact.save()
    # change last modified to now
    resume.last_modified=timezone.now().date()
    return contact

def service_set_skills(skills):
    # get resume from database
    resume=Resume.objects.get(pk=skills['resume_id'])
    # create skills object with resume as parameter
    skills=Skills(
        keywords=skills['keywords'],
        resume=resume
    )
    # save to database
    skills.save()
    # change last modified to now
    resume.last_modified=timezone.now().date()
    resume.save()
    return skills


def service_delete_resume(resume_id):
    resume=Resume.objects.get(pk=resume_id)
    resume.delete()

def service_delete_section(section_id):
    section=Section.objects.get(pk=section_id)
    section.resume.last_modified=datetime.now()
    section.resume.save()
    section.delete()

def service_delete_subsection(subsection_id):
    subsection=SubSection.objects.get(pk=subsection_id)
    subsection.section.resume.last_modified=datetime.now()
    subsection.section.resume.save()
    subsection.delete()

def service_delete_contact(contact_id):
    contact=Contact.objects.get(pk=contact_id)
    contact.resume.last_modified=datetime.now()
    contact.resume.save()
    contact.delete()

def service_delete_skills(skills_id):
    skills=Skills.objects.get(pk=skills_id)
    skills.resume.last_modified=datetime.now()
    skills.resume.save()
    skills.delete()


def service_update_resume(resume):
    old_resume=Resume.objects.get(pk=resume['resume_id'])
    old_resume.name=resume['name']
    old_resume.job_description=resume['job_description']
    old_resume.last_modified=timezone.now().date()
    old_resume.save()
    return old_resume

def service_update_section(section):
    old_section=Section.objects.get(pk=section['section_id'])
    old_section.name=section['name']

    old_section.resume.last_modified=timezone.now().date()
    old_section.resume.save()
    old_section.save()
    return old_section

def service_update_subsection(subsection):
    old_subsection=SubSection.objects.get(pk=subsection['subsection_id'])
    old_subsection.name=subsection['name']
    started_at=datetime.strptime(subsection['started_at'],DATE_FORMAT).date() 
    ended_at=datetime.strptime(subsection['ended_at'],DATE_FORMAT).date() 
    old_subsection.started_at=started_at
    old_subsection.ended_at=ended_at
    old_subsection.organization=subsection['organization']
    old_subsection.description=subsection['description']
    # update last modified
    old_subsection.section.resume.last_modified=timezone.now().date()
    old_subsection.section.resume.save()
    old_subsection.save()
    return old_subsection

def service_update_contact(contact):
    old_contact=Contact.objects.get(pk=contact['contact_id'])
    old_contact.first_name=contact['first_name']
    old_contact.last_name=contact['last_name']
    old_contact.phone=contact['phone']
    old_contact.address=contact['address']
    old_contact.email=contact['email']
    # 
    old_contact.resume.last_modified=timezone.now().date()
    old_contact.resume.save()
    old_contact.save()
    return old_contact

def service_update_skills(skills):
    old_skills=Skills.objects.get(pk=skills['skills_id'])
    old_skills.keywords=skills['keywords']

    old_skills.resume.last_modified=timezone.now().date()
    
    old_skills.resume.save()
    old_skills.save()
    return old_skills


def service_get_resume_by_user(user_id):
    resumes=Resume.objects.filter(profile=user_id)
    return list(resumes)


def service_set_summary(resumeId,summary):
    resume=Resume.objects.get(pk=resumeId)
    resume.summary=summary
    resume.save()
    return resume

def service_get_resume_by_id(resume_id):
    # generate pdf
    resume=Resume.objects.get(pk=resume_id)
    if(not resume):
        return False
    resume=ResumeSerializer(resume,many=False).data
    return resume