from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from ..services.auth import service_login,service_signup
from django.views.decorators.csrf import csrf_exempt
from ..security import auth_required
@csrf_exempt
@api_view(['POST'])
def login(req):
    email=req.data['email']
    password=req.data['password']
    value=service_login(email,password)
    if(value):
        return Response({'token':value})
    return Response({"message":"email or password incorrect"},status=status.HTTP_401_UNAUTHORIZED)

@csrf_exempt
@api_view(['POST'])
def register(req):# add status codes
    result=service_signup(req.data)
    if(result):
        return Response({"message":"success"})
    return Response({"message":"failure"},status=status.HTTP_409_CONFLICT)


@auth_required
@api_view(['GET'])
def validate(req):
    return Response({"message":"success"})