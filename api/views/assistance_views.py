from rest_framework.decorators import api_view
from rest_framework.response import Response
from ..services.assitance import summary,serviceMatchResume

@api_view(['POST'])
def summarize(req):
    resumeId=req.data['resumeId']
    value=summary(resumeId)
    return Response({"summary":value})


@api_view(['POST'])
def matchResume(req):
    score=serviceMatchResume(req.data['resumeId'])
    if(score):
        return Response({"success":True,"score":score})
    else:
        return Response({"success":False})