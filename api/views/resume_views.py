from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from xhtml2pdf import pisa

from ..serializers import ResumeSerializer,SectionSerializer,SubSectionSerializer,ContactSerializer,SkillsSerializer
from ..security import auth_required

from io import BytesIO

from ..services.resume import service_create_resume,service_update_resume,service_add_section,service_add_subsection,service_set_summary
from ..services.resume import service_set_contact,service_update_section,service_get_resume_by_user,service_update_subsection
from ..services.resume import service_update_contact,service_set_skills,service_update_skills,service_delete_resume
from ..services.resume import service_delete_section,service_delete_contact,service_delete_skills,service_delete_subsection,service_get_resume_by_id
from django.shortcuts import render
from django.http import FileResponse
from django.template.loader import get_template
from django.template import Context


# {
#     "name":"",
#     "job_description":""
# }
@auth_required
@api_view(['POST'])
def create_resume(req):
    values=req.data
    user_id=req.session['user']['profile_id']
    values['user_id']=user_id
    resume=service_create_resume(values)
    resume=ResumeSerializer(resume).data
    return Response(resume)
# {
#     "name":"",
#     "job_description":"",
#     "resume_id":1
# }
@auth_required
@api_view(['PUT'])
def update_resume(req):
    values=req.data
    resume=service_update_resume(values)
    resume=ResumeSerializer(resume).data
    return Response(resume)

# {
#     "resume_id":1
# }
@auth_required
@api_view(['DELETE'])
def delete_resume(req):
    resume_id=req.data["resume_id"]
    try:
        service_delete_resume(resume_id)
        return Response({"message":"success"})
    except:
        return Response({"message":"error"},status=status.HTTP_404_NOT_FOUND)



# {
#     "resume_id":1,
#     "name":""
# }
@auth_required
@api_view(['POST'])
def add_section(req):
    values=req.data
    section=service_add_section(values)
    section=SectionSerializer(section).data
    return Response(section)
# {
#     "section_id":1,
#     "name":""
# }
@auth_required
@api_view(['PUT'])
def update_section(req):
    values=req.data
    section=service_update_section(values)
    section=SectionSerializer(section).data
    return Response(section)

# {
#     "section_id":1
# }
@auth_required
@api_view(['DELETE'])
def delete_section(req):
    section_id=req.data['section_id']
    try:
        service_delete_section(section_id)
        return Response({"message":"success"})
    except:
        return Response({"message":"error"},status=status.HTTP_404_NOT_FOUND)


# {
#     "section_id":1,
#     "started_at":"year-month-day",
#     "ended_at":"year-month-day",
#     "name":"",
#     "organization":"",
#     "description":"",
# }
@auth_required
@api_view(['POST'])
def add_subsection(req):
    values=req.data
    subsection=service_add_subsection(values)
    subsection=SubSectionSerializer(subsection).data
    return Response(subsection)

# {
#     "subsection_id":1,
#     "started_at":"year-month-day",
#     "ended_at":"year-month-day",
#     "name":"",
#     "organization":"",
#     "description":"",
# }
@auth_required
@api_view(['PUT'])
def update_subsection(req):
    values=req.data
    subsection=service_update_subsection(values)
    subsection=SubSectionSerializer(subsection).data
    return Response(subsection)

# {
#     "subsection_id":1
# }
@auth_required
@api_view(['DELETE'])
def delete_subsection(req):
    subsection_id=req.data['subsection_id']
    try:
        service_delete_subsection(subsection_id)
        return Response({"message":"success"})
    except:
        return Response({"message":"error"},status=status.HTTP_404_NOT_FOUND)

# {
#     "resume_id":1,
#     "linked_in":"",
#     "github":"",
#     "phone":"",
#     "personal_website":""
# }
@auth_required
@api_view(['POST'])
def add_contact(req):
    values=req.data
    print(values)
    contact=service_set_contact(values)
    
    contact=ContactSerializer(contact).data
    return Response(contact)

# {
#     "contact_id":1,
#     "linked_in":"",
#     "github":"",
#     "phone":"",
#     "personal_website":""
# }
@auth_required
@api_view(['PUT'])
def update_contact(req):
    values=req.data
    contact=service_update_contact(values)
    contact=ContactSerializer(contact).data
    return Response(contact)

# {
#     "contact_id":1
# }
@auth_required
@api_view(['DELETE'])
def delete_contact(req):
    contact__id=req.data['contact_id']
    try:
        service_delete_contact(contact__id)
        return Response({"message":"success"})
    except:
        return Response({"message":"error"},status=status.HTTP_404_NOT_FOUND)

# {
#     "resume_id":1,
#     "keywords":""
# }
@auth_required
@api_view(['POST'])
def add_skills(req):
    values=req.data
    skills=service_set_skills(values)
    skills=SkillsSerializer(skills).data
    return Response(skills)

# {
#     "skills_id":1,
#     "keywords":""
# }
@auth_required
@api_view(['PUT'])
def update_skills(req):
    values=req.data
    skills=service_update_skills(values)
    skills=SkillsSerializer(skills).data
    return Response(skills)

# {
#     "skills_id":1
# }
@auth_required
@api_view(['DELETE'])
def delete_skills(req):
    skills_id=req.data['skills_id']
    try:
        service_delete_skills(skills_id)
        return Response({"message":"success"})
    except:
        return Response({"message":"error"},status=status.HTTP_404_NOT_FOUND)

# no parameters
@auth_required
@api_view(['GET'])
def get_all_resume(req):
    # get user from token
    user_id=req.session['user']['profile_id']
    # call service bu user id
    resumes=service_get_resume_by_user(user_id)
    # return all resume
    resumes=ResumeSerializer(resumes,many=True).data
    return Response(resumes)

# {
#     "resumeId":5,
#     "summary":""
# }
@auth_required
@api_view(['POST'])
def set_summary(req):
    resumeId=req.data['resumeId']
    summary=req.data['summary']
    resume=service_set_summary(resumeId,summary)
    resume=ResumeSerializer(resume,many=False).data
    return Response(resume)

@api_view(['GET'])
def get_resume(req):
    data={}
    return render(req,"resume.html",data)

@api_view(['POST'])
def export_pdf_resume(req):
    resume=service_get_resume_by_id(req.data['resumeId'])

    # put values for resume template
    template=get_template("resume.html")

    html=template.render(resume)
    ## error here
    pdfFile = BytesIO()
    status=pisa.CreatePDF(html,pdfFile)
    pdfFile.seek(0)
    #########
    if(status.err):
        return Response({"error":"error"})
    else:
        filename=f"{resume['name']}.pdf"
        response = FileResponse(pdfFile,as_attachment=True,filename=filename, content_type='application/pdf')
        return response
