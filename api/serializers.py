from rest_framework import serializers
from .models import Profile,Resume,Contact,Skills,Section,SubSection

class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model=Contact
        fields=("first_name","last_name","email","phone","address","contact_id")

class SkillsSerializer(serializers.ModelSerializer):
    class Meta:
        model=Skills
        fields=("skills_id","keywords")

class SubSectionSerializer(serializers.ModelSerializer):
    class Meta:
        model=SubSection
        fields=("subsection_id","name","started_at","ended_at","organization","description")

class SectionSerializer(serializers.ModelSerializer):
    subsections=SubSectionSerializer(many=True,read_only=True)
    class Meta:
        model=Section
        fields=("section_id","name","subsections")

class ResumeSerializer(serializers.ModelSerializer):
    sections=SectionSerializer(many=True,read_only=True)
    skills=SkillsSerializer(many=True,read_only=True)
    contact=ContactSerializer(many=True,read_only=True)
    class Meta:
        model=Resume
        fields=("resume_id","name","job_description","match","created_at","last_modified","skills","contact","sections","summary")

class ProfileSerializer(serializers.ModelSerializer):
    resumes=ResumeSerializer(many=True)
    class Meta:
        model=Profile
        fields=("profile_id","first_name","last_name","email","resumes")

class FullProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model=Profile
        fields=("profile_id","first_name","last_name","email","password")
